#ifndef NUMERIC_H
#define NUMERIC_H
#include <stdlib.h>
#include <stdio.h>
int gcd(int a, int b);
int gcdI(int *arg);
int* gcdOther(int a, int b);
int* gcdOtherI(int *arg);
int* gcdGeneral(int x, int y, int a, int b);
int* gcdGeneralI(int *arg);
int lcm(int a, int b, int gcd);
int lcmI(int *arg);
int* gcdLinear(int a, int b);
int* gcdLinearI(int *arg);
int gcdIsSolvable(int a, int b);
int gcdIsSolvableI(int *arg);
int exponent(int a, int b, int n);
int exponentI(int *arg);
int* quotient(int a, int b);
int* quotientI(int *arg);
int probablyPrime(int count, int m);
int probablyPrimeI(int *arg);
void printResult(int *a, int count);
#endif
