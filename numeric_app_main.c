#include "numeric.h"

int main(int argc, char **argv) {
  int option, r, *R;
  int rLen, inCount;
  int *in, *inFrom, *inTo;

  if(argc < 4) {
    return 1;
  }

  argv++;
  option = atoi(*argv); // option number
  argv++;
  inCount = atoi(*argv); // in count
  argv++;
  rLen = atoi(*argv); // r len
  argv++;
  in = calloc(inCount, sizeof(int));
  inFrom = in;
  inTo = inFrom + inCount;
  for(; inFrom < inTo; inFrom++, argv++) {
    *inFrom = atoi(*argv);
  }

  if(option == 1) {
    r = gcdI(in);
    printf("%d \n", r);
    goto END;
  }

  if(option == 2) {
    R = gcdLinearI(in);
    printResult(R, rLen);
    free(R);
    goto END;
  }

  if(option == 3) {
    r = gcdIsSolvableI(in);
    printf("%d \n", r);
    goto END;
  }

  if(option == 4) {
    R = gcdGeneralI(in);
    printResult(R, rLen);
    free(R);
    goto END;
  }

  if(option == 5) {
    r = lcmI(in);
    printf("%d \n", r);
    goto END;
  }

  if(option == 6) {
    r = exponentI(in);
    printf("%d \n", r);
    goto END;
  }

  if(option == 7) {
    r = probablyPrimeI(in);
    printf("%d \n", r);
    goto END;
  }

  END: free(in);

  return 0;
}
