#include "numeric_test.h"

int main() {
  testGcdOne();
  testGcdMoreThanOne();
  testGcdLinearOne();
  testGcdLinearMoreThanOne();
  testGcdIsSolvableYes();
  testGcdIsSolvableNo();
  testGcdOther();
  testGcdGeneral();
  testLcmOne();
  testLcmOther();
  testExponentPrime();
  testExponentComposite();
  testQuotientRPositive();
  testQuotientRZero();
  testProbablyPrimeYes();
  testProbablyPrimeNo();
  return 0;
}
