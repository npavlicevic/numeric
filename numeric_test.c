#include "numeric_test.h"
void testGcdOne() {
  assert(gcd(55, 34) == 1);
}
void testGcdMoreThanOne() {
  assert(gcd(555, 111) == 111);
}
void testGcdLinearOne() {
  int *linear;
  linear = gcdLinear(155, 47);
  assert(linear[0] == -10);
  assert(linear[1] == 33);
  free(linear);
}
void testGcdLinearMoreThanOne() {
  int *linear;
  linear = gcdLinear(155, 35);
  assert(linear[0] == -2);
  assert(linear[1] == 9);
  free(linear);
}
void testGcdIsSolvableYes() {
  assert(!gcdIsSolvable(55, 11));
}
void testGcdIsSolvableNo() {
  assert(gcdIsSolvable(71, 7));
}
void testGcdOther() {
  int *linear;
  linear = gcdOther(155, 35);
  assert(linear[0] == 5);
  assert(linear[1] == -2);
  assert(linear[2] == 9);
  free(linear);
}
void testGcdGeneral() {
  int *linear;
  linear = gcdGeneral(-2, 9, 155, 35);
  assert(linear[0] == -2);
  assert(linear[1] == 35);
  assert(linear[2] == 9);
  assert(linear[3] == -155);
  free(linear);
}
void testLcmOne() {
  assert(lcm(12, 20, 4) == 60);
}
void testLcmOther() {
  assert(lcm(15, 21, 3) == 105);
}
void testExponentPrime() {
  assert(exponent(5, 2, 7) == 4);
}
void testExponentComposite() {
  assert(exponent(5, 3, 49) == 27);
}
void testQuotientRPositive() {
  int *linear;
  linear = quotient(5, 3);
  assert(linear[0] == 1);
  assert(linear[1] == 2);
  free(linear);
}
void testQuotientRZero() {
  int *linear;
  linear = quotient(14, 7);
  assert(linear[0] == 2);
  assert(linear[1] == 0);
  free(linear);
}
void testProbablyPrimeYes() {
  assert(!probablyPrime(10, 37));
}
void testProbablyPrimeNo() {
  assert(probablyPrime(10, 40));
}
