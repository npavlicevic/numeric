#include "numeric.h"

/*
* Function: gcd
* 
* find greatest common divisor
*
* return: integer
*/
int gcd(int a, int b) {
  int n = a; // number
  int c = b; // current
  int r = n % c; // remainder

  for(;r>0;) {
    n=c;
    c=r;
    r = n % c;
  }
  return c;
}
/*
* Function: gcdI
* 
* interface for generated application
*
* return: integer
*/
int gcdI(int *arg) {
  return gcd(*arg, *(arg + 1));
}
/*
* Function: gcdOther
* 
* another algorithm to find gcd and linear combination
*
* return: integer
*/
int* gcdOther(int a, int b) {
  int x = 1; // x quantity
  int g = a; // gcd
  int v = 0;
  int vv = b;
  int y; // y quantity
  int q;
  int t;
  int s;
  int *r = (int*)calloc(3, sizeof(int));

  for(;vv;) {
    q = g/vv;
    t = g%vv;
    s = x - q*v;
    g = vv;
    x = v;
    y = vv;
    v = s;
    vv = t;
  }

  y = (g - a*x)/b;
  r[0] = g;
  r[1] = x;
  r[2] = y;

  return r;
}
/*
* Function: gcdOtherI
* 
* interface for generated application
*
* return: integer
*/
int* gcdOtherI(int *arg) {
  return gcdOther(*(arg), *(arg + 1));
}
/*
* Function: gcdGeneral
* 
* find general solution to linear diophantine equation
*
* return: integer
*/
int* gcdGeneral(int x, int y, int a, int b) {
  int *r = (int*)calloc(4, sizeof(int));
  r[0] = x;
  r[1] = b;
  r[2] = y;
  r[3] = -a;
  return r;
}
/*
* Function: gcdGeneralI
* 
* interface for generated application
*
* return: integer
*/
int* gcdGeneralI(int *arg) {
  return gcdGeneral(*(arg), *(arg+1), *(arg+2), *(arg+3));
}
/*
* Function: lcm
* 
* find least common multiple
*
* return: integer
*/
int lcm(int a, int b, int gcd) {
  return gcd*(a/gcd)*(b/gcd);
}
/*
* Function: lcmI
* 
* interface for generated application
*
* return: integer
*/
int lcmI(int *arg) {
  return lcm(*arg, *(arg + 1), *(arg + 2));
}
/*
* Function: gcdLinear
* 
* find greatest common divisor as linear combination
*
* return: integer
*/
int* gcdLinear(int a, int b) {
  int n = a; // number
  int c = b; // current
  int q = n / c; // quotient
  int r = n % c; // remainder
  int *linear = (int*)calloc(2, sizeof(int));
  int x_t, x_1, x_2;
  int y_t, y_1, y_2;

  linear[0] = 0;
  linear[1] = c;

  if(r == 0) {
    return linear;
  }

  x_1 = 1; x_2 = 1;
  y_1 = -q; y_2 = -q;

  linear[0] = x_2;
  linear[1] = y_2;

  if(r == 1) {
    return linear;
  }

  n=c;
  c=r;
  q = n / c;
  r = n % c;
  if(r == 0) {
    return linear;
  }

  x_2 = x_2 * (-q);
  y_2 = y_2 * (-q) + 1;

  for(;r;) {
    n=c;
    c=r;
    q = n / c;
    r = n % c;
    if(r < 1) {
      break;
    }
    x_t = x_2;
    y_t = y_2;
    x_2 = x_2 * (-q) + x_1;
    y_2 = y_2 * (-q) + y_1;
    x_1 = x_t;
    y_1 = y_t;
  }

  linear[0] = x_2;
  linear[1] = y_2;

  return linear;
}
/*
* Function: gcdLinearI
* 
* interface for generated application
*
* return: integer
*/
int* gcdLinearI(int *arg) {
  return gcdLinear(*arg, *(arg + 1));
}
/*
* Function: gcdIsSolvable
* 
* check if diophantine equation is solvable
*
* return: integer
*/
int gcdIsSolvable(int a, int b) {
  return a % b;
}
/*
* Function: gcdIsSolvableI
* 
* interface for generated application
*
* return: integer
*/
int gcdIsSolvableI(int *arg) {
  return gcdIsSolvable(*arg, *(arg + 1));
}
/*
* Function: exponent
* 
* find exponent in terms of a modulus
*
* return: integer
*/
int exponent(int a, int b, int n) {
  int c = 1; // current
  int m = a % n; // multiplier
  unsigned int mask = 0x00000001;
  for(;b>0;) {
    if(b & mask) {
      c = (c * m) % n;
    }
    b >>= 1;
    m = (m * m) % n;
  }
  return c;
}
/*
* Function: exponentI
* 
* interface for generated application
*
* return: integer
*/
int exponentI(int *arg) {
  return exponent(*(arg), *(arg + 1), *(arg + 2));
}
/*
* Function: quotient
* 
* result of division in terms of quotient and remainder
*
* return: integer
*/
int* quotient(int a, int b) {
  int q = 0;
  int r = abs(a);
  int *linear = (int*)calloc(2, sizeof(int));
  for(;r >= b;) {
    r -= b;
    q++;
  }
  if(a < 0 && r > 0) {
    r = b - r;
    q = -(q+1);
  }
  linear[0] = q;
  linear[1] = r;

  return linear;
}
/*
* Function: quotientI
* 
* interface for generated application
*
* return: integer
*/
int* quotientI(int *arg) {
  return quotient(*arg, *(arg + 1));
}
/*
* Function: probablyPrime
* 
* check if m is a prime
*
* if it is then a^(m-1) = 1 mod m
*
* return: integer
*/
int probablyPrime(int count, int m) {
  int item;
  int a;
  int i;

  for(i=0; i<count; i++) {
    a=rand()%m;
    item=exponent(a, m-1, m);
    if(item>1) {
      return item;
    }
  }

  return 0;
}
/*
* Function: probablyPrimeI
* 
* interface for generated application
*
* return: integer
*/
int probablyPrimeI(int *arg) {
  return probablyPrime(*(arg), *(arg + 1));
}
/*
* Function: printResult
* 
* print result vector
*
* return: void
*/
void printResult(int *a, int count) {
  int *start = a;
  int *end = a + count;
  
  for(;start != end; start++) {
    printf("%d ", *start);
  }

  printf("\n");
}
