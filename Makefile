#
# Makefile 
# number theoretic methods
#

CC=gcc
AR=ar
CFLAGS=-g -std=c99 -Werror -Wall -Wextra -Wformat -Wformat-security -pedantic
OBJ_FLAGS=-c -static
AR_FLAGS=rcs
LIBS=-lm

FILES=numeric.h numeric.c numeric_test.h numeric_test.c numeric_main.c numeric_app_main.c
CLEAN=numeric_main numeric_app_main

all: main obj lib app

main: ${FILES}
	${CC} ${CFLAGS} ${LIBS} numeric.c numeric_test.c numeric_main.c -o numeric_main

obj: ${FILES}
	${CC} ${OBJ_FLAGS} numeric.c -o numeric.o

lib: ${FILES}
	${AR} ${AR_FLAGS} libnumeric.a numeric.o

app: ${FILES}
	${CC} ${CFLAGS} ${LIBS} numeric.c numeric_app_main.c -o numeric_app_main

appeg: ${FILES}
	./numeric_app_main 1 2 2 5 3

clean: ${CLEAN}
	rm $^
